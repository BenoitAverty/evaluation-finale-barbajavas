# Eval

## Consignes et conseils

**Lisez intégralement le document avant de démarrer.**

Pour cette évaluation, on vous demande de développer une application de zéro.

Le cahier des charges est découpé en différents « Chantiers » regroupant plusieurs fonctionnalités qui partagent un même « thème ».

**Il n'est pas nécessaire de finir toutes les fonctionnalités d'un chantier avant de passer au suivant**, vous êtes libres de choisir l'ordre
dans lequel vous abordez ces différents chantiers. En revanche, au sein d'un chantier, les fonctionnalités sont classés par ordre grossier de
priorité.

### Livrable

Un repo git contenant le front et le back (ainsi que les pipelines, dockerfiles...) ou deux repos git : un pour le front et un pour le back.
De préférence dans votre espace personnel gitlab. Envoyez votre repo (ou vos repos) sur slack **dès le début de l'évaluation** en message privé à tous vos formateurs (Gwénolé, Benoit A, Benoit V, Sylvain, Nicolas et Adrien) et à Norbert.

L'historique git sera valorisé : on essaie de faire des commits ayant du sens et des branches pour chaque chantier ou chaque tâche.

Les tests automatisés seront valorisés également s'ils existent et qu'ils passent :)

### Barême

Répartition approximative de la notation :

- 50% sur le backend (Java, Spring, Postgres, JPA...) ;
- 30% pour l'application front (Javascript, Angular) ;
- 20% pour la pipeline, les dockerfiles, ...

Il n'y a pas de barème précis pour chaque tâche. Le cahier des charges est volontairement trop long pour la durée de l'évaluation, ce qui vous permet (en partie) de choisir sur quelles fonctionnalités travailler.

**Le critère esthétique n'a pas d'importance**. On préfère une interface qui fonctionne et on ne demande pas nécessairement une interface « belle ».

La qualité de votre présentation aura forcément un impact sur la notation, car elle mettra en valeur les éléments que vous choisirez de montrer (nous n'aurons pas le temps d'inspecter tout votre code en détail...).

### Conseils

* Lisez tout le cahier des charges avant de démarrer.
* Le cahier des charges est volontairement trop long. Pas d'inquiétude si vous n'en codez qu'une partie/
* **Demandez de l'aide**. Nous ne sommes pas intéressés par votre capacité à mémoriser des noms de méthodes ou des astuces partagées.
  rapidement il y a trois semaines... Demandez de l'aide autant que lors des TPs hors évaluation.
* N'hésitez pas à prendre les tâches dans le désordre quand elles sont indépendantes.
* Terminez les tâches plutôt que démarrer beaucoup de tâches.
* Mais n'hésitez pas à faire des choses dans le back qui ne sont pas accessibles depuis le front : vous montrerez comment ça marche avec Insomnia et ça sera déjà bien !

### Soutenance

Le dernier jour, vous présenterez votre application devant 4 développeurs. Le format de la soutenance est libre, l'objectif étant de mettre
en valeur au mieux le travail fourni. L'idéal est de montrer l'application finale en fonctionnement (via le front ou l'API Rest) mais vous pouvez également montrer :

 - Un diagramme de classes.
 - Un modèle conceptuel de données.
 - Le script SQL de création de la base de données.
 - Une partie du code de votre application qui vous semble particulièrement intéressant ou qui a demandé beaucoup d'effort.
 - ...

Pensez également à montrer si ça vous semble pertinent :

- La pipeline d'intégration continue La façon dont vous construisez et démarrez votre application (avec maven, avec docker...).
- Le résultat des tests unitaires.

## Sujet: Site de questions / réponses

La Zenika Academy souhaite développer un outil personnalisé pour poser des questions type FAQ (Foire Aux Questions). Cet outil servira à partager des infos entre étudiants des prochaines promotions.

Il est recommandé de lire l'intégralité de ce document avant de démarrer les développements.

### Description générale de l'application

L'outil présentera des fonctionnalités similaires à une plateforme type [StackOverflow](https://stackoverflow.com/).

L'application devra permettre à des utilisateurs de poser des questions et d'y répondre. Les questions sont ensuite disponibles pour tous les visiteurs, même s'ils n'ont pas de compte utilisateur. Pour répondre aux questions posées il est nécessaire de disposer d'un compte utilisateur. 

Afin de faciliter la découverte de toutes les questions et des réponses associées, des systèmes de recherche et de tags sont à prévoir sur les questions. On envisage aussi un système de notations sur les réponses afin de disposer des réponses les plus pertinentes aka stackoverflow.

### Cahier des charges fonctionnel

Dans ce cahier des charges, on parle de plusieurs catégories de personnes : 
 - Un « Visiteur » est une personne anonyme qui consulte le site (questions/réponses visibles en lecture seule).
 - Un « Utilisateur » est une personne qui écrit des questions ou qui répond a des questions.
 
Pour simplifier les phrases, on considère qu'un utilisateur est aussi un visiteur (il peut faire tout ce que peut faire un visiteur).  

> La partie « Utilisateur » est un chantier a lui seul, vous pouvez dans un premier temps permettre au visiteur de disposer de tous les droits.

### Chantier « Écriture et affichage des questions/réponses »

Une question contient les données suivantes : 
 * Un titre ;
 * Un contenu ;
 * Une date.
Le titre ne doit pas dépasser les 20 mots et doit se terminer par un `?`.

Une réponse contient les données suivantes : 
 * Un contenu ;
 * Une date.
Le contenu de la réponse ne doit pas dépasser les 100 mots.

#### Fonctionnalités principales

* Un utilisateur peut soumettre une question. 
* Un utilisateur peut répondre à une question.
* Un visiteur peut afficher la liste des questions (triée par ordre chronologique).
* Un visiteur peut afficher le détail d'une question et donc les réponses associées.

#### Fonctionnalités « Nice to have »

* Un utilisateur peut voter pour une réponse.
* À l'affichage du détail d'une question, les réponses sont affichées avec le nombre de votes et triées par ordre de votes.
* Les questions seront affichées sur plusieurs pages numérotées [Spring Data PagingAndSortingRepository doc reference](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.core-concepts).
 
### Chantier « Authentification des utilisateurs »

On souhaite enregistrer les informations sur les auteurs des questions et des réponses, ainsi que gérer quelques permissions.

Lorsqu'une question est posée, on enregistre l'auteur pour référence. L'auteur d'une question et d'une réponse ne peut pas être modifié. L'auteur d'une question peut répondre à sa propre question. Mais ne peut pas voter pour sa propre réponse (nice to have du chantier precedent).

#### Fonctionnalités principales

* Pouvoir gérer des utilisateurs.
* Mémoriser l'auteur d'une question et d'une réponse. Les questions et les réponses doivent nécessairement disposer d'un auteur.
* Seul un utilisateur peut soumettre une question et une réponse. Le visiteur est en lecture seule.
 
#### Fonctionnalités « nice to have »

* Un utilisateur authentifié peut voter une seule fois sur chaque réponse (hormis la sienne). Il peut également voter pour toutes les réponses d'une question.
* Un utilisateur peut « annuler » son vote sur une réponse (mais seulement le sien !).
* Afficher l'adresse mail de l'utilisateur pour éventuellement lui envoyer des mails.
 
### Chantier « Recherche et Navigation »

L'objectif à moyen terme est d'héberger un grand nombre de questions sur la plateforme. En conséquence, on souhaite fournir un système de recherche des questions pour faciliter la découverte des questions et des réponses.

#### Fonctionnalités principales

* Un visiteur peut rechercher une question via son titre. La recherche doit trouver les questions dont le titre peut correspondre que partiellement
* Les questions devront maintenant être rattachées à un langage (/!\ champ supplémentaire). La liste des langages est paramétrée préalablement à la création des questions
* Un visiteur peut visualiser la liste des questions liées à un langage donnée.
 
#### Fonctionnalités « Nice to have »

* Un visiteur peut visualiser la liste des questions et des réponses d'un utilisateur.
* Un visiteur peut visualiser la liste des questions qui n'ont pas de réponse.

### Chantier « Modération »

L'objectif à long terme est d'héberger un grand nombre de questions sur la plateforme. En conséquence, on souhaite modérer les questions et les réponses afin de supprimer celles qui posent problème.

#### Fonctionnalités principales

* Un utilisateur peut signaler (une seule fois) une question ou une réponse (un « flag » est présent sur le dit contenu problématique).
* Les questions et réponses signalées ne sont pas visibles par un visiteur.
* Permettre la suppression du contenu signalé : 
  * Supprimer une question signalée supprime les réponses associées.
  * Supprimer une réponse signalée ne supprime pas la question associée.
 
#### Fonctionnalités « Nice to have »

* Permettre la notion de rôle au sein des utilisateurs (« User » et « Admin »).
* Le signalement n'est visible que par l'utilisateur qui signale et par un utilisateur de role « Admin ».
* Les admins ont accès à une liste de contenu signalé.
* Seuls les admins peuvent supprimer du contenu.
